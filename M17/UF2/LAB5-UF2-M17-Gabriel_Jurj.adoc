= LAB5: Hardening 
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

== Enviorment preparations.

.We'll start by installing apparmor-profiles.

[source,bash]
----
gjurj@gj-asix:~$ sudo apt install apparmor-profiles
----

.We'll also need to install nginx and apparmor-utils

[source,bash]
----
gjurj@gj-asix:~$ sudo apt install nginx && sudo apt install apparmor-utils
----

.Once everything is installed we can proceed with the setup of our experimental webs.
.For startes we'll create two different directories one for secure enviorments and another for unsecure.
[source,bash]
----
gjurj@gj-asix:~$ sudo mkdir /var/www/safe
gjurj@gj-asix:~$ sudo mkdir /var/www/unsafe
----

.In the safe directory will create an index.html file. 

[source,bash]
----
gjurj@gj-asix:/var/www/safe$ cat index.html
<html>
<h2>This enviorment is safe</h2>
</html>

gjurj@gj-asix:/var/www/safe$
----

.Now we'll do tha same but for the unsafe enviorment.

[source,bash]
----
gjurj@gj-asix:/var/www/safe$ cat index.html
<html>
<h2>This enviorment is NOT safe</h2>
</html>

gjurj@gj-asix:/var/www/safe$
----

.Now we need to edit nginx.conf file, first we need to comment the following line.

[source]
----
include /etc/nginx/sites-enabled/*;
----

.Now we need to add the following under the include line.

[source]
----
#include /etc/nginx/modules-enabled/*.conf;
server {
        listen 8080;
        location / {
        root /var/www;
        }
}
----

.We'll need to restart the nginx service .

[source,bash]
----
sudo systemctl restart nginx.service
----

.Now we'll need to check that everything is working acordingly by connecting accesing the webs we created. 
.We can use elinks command from another client that has connection to our server.
.Note the server IP is 192.168.1.61 and the server listens through port 8080. 
[source,bash]
----
elinks http://192.168.1.61:8080/safe
elinks http://192.168.1.61:8080/unsafe
----


== Apparmor config.

.We'll start by going to the apparmore directory /etc/apparmor.d.
.Once in we'll execute the following command.

[source,bash]
----
gjurj@gj-asix:/etc/apparmor.d$ sudo aa-autodep nginx
----

.The result of the command should show the following.

[source,bash]
----
Writing updated profile for /usr/sbin/nginx.
----

.As we are still in profile creation procees we'll need to use the following command to put the profile in complain mode.

[source,bash]
----
gjurj@gj-asix:/etc/apparmor.d$ sudo aa-complain nginx
Setting /usr/sbin/nginx to complain mode.
----

.Now we'll use the following command to check the logs and update the profile we just created.
.We'll answer A for the following questions and S to save the changes.

[source,bash]
----
gjurj@gj-asix:/etc/apparmor.d$ sudo aa-logprof
Reading log entries from /var/log/syslog.
Updating AppArmor profiles in /etc/apparmor.d.
Complain-mode changes:

Profile:  /usr/sbin/sssd
Path:     /proc/956/cmdline
New Mode: owner r
Severity: 6

 [1 - owner /proc/*/cmdline r,]
  2 - owner /proc/956/cmdline r,
(A)llow / [(D)eny] / (I)gnore / (G)lob / Glob with (E)xtension / (N)ew / Audi(t) / (O)wner permissions off / Abo(r)t / (F)inish
Adding owner /proc/*/cmdline r, to profile.

Profile:  /usr/sbin/sssd
Path:     /proc/995/cmdline
Old Mode: owner r
New Mode: r
Severity: 6

 [1 - /proc/*/cmdline r,]
  2 - /proc/995/cmdline r,
(A)llow / [(D)eny] / (I)gnore / (G)lob / Glob with (E)xtension / (N)ew / Audi(t) / Abo(r)t / (F)inish
Adding /proc/*/cmdline r, to profile.
Deleted 1 previous matching profile entries.

= Changed Local Profiles =

The following local profiles were changed. Would you like to save them?

 [1 - /usr/sbin/sssd]
(S)ave Changes / Save Selec(t)ed Profile / [(V)iew Changes] / View Changes b/w (C)lean profiles / Abo(r)t
Writing updated profile for /usr/sbin/sssd.
----

.Now we need to edit the following file and add the following.

[source,bash]
----
gjurj@gj-asix:~$ sudo nano /etc/apparmor.d/usr.sbin.nginx
# Last Modified: Wed Jan 12 09:15:25 2022
#include <tunables/global>

/usr/sbin/nginx {
  #include <abstractions/base>
  #include <abstractions/lxc/container-base>
  capability dac_override,
  capability dac_read_search,
  capability net_bind_service,
  capability setgid,
  capability setuid,

  /var/www/safe/* r,
  deny /var/www/unsafe/* r,
  /etc/group r,
  /etc/nginx/conf.d/ r,
  /etc/nginx/mime.types r,
}
gjurj@gj-asix:~$
----

.One more thing before we finish we need to change the status from complain to enforce using the following command.

[source,bash]
----
gjurj@gj-asix:/etc/apparmor.d$ sudo aa-enforce nginx
Setting /usr/sbin/nginx to enforce mode.
----
.If thers an error like the following, to solve it we'll need to install lxc package. 

[source,bash]
----
gjurj@gj-asix:/etc/apparmor.d$ sudo aa-enforce nginx

ERROR: Include file /etc/apparmor.d/abstractions/lxc/container-base not found
----

.Finally we'll reload the service of apparmor, and restart nginx service

[source,bash]
----
gjurj@gj-asix:~$ sudo /etc/init.d/apparmor reload
gjurj@gj-asix:~$ sudo systemctl restart nginx.service
----

